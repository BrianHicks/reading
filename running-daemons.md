# [![](/images/cuetech.png)](/)

## [The technology that powers Cue](/)

  * [Blog](/)
  * [Archives](/blog/archives)

# Daemon Showdown: Upstart vs. Runit vs. Systemd vs. Circus vs. God

Mar 8th, 2013

We write a lot of [daemons](http://en.wikipedia.org/wiki/Daemon_(computing)):
programs which run on servers in the background, like an HTTP server, or a
database. Once we've written the programs, though, we have to run them, and
running programs as daemons is surprisingly heavy on details; it's fraught
with perils for the unwary. If you go the traditional Unix way, you do some
magic with double-forking and pid files and init scripts, and it's horribly
tedious. If you run in a detached session of
[screen](http://www.gnu.org/software/screen/) or
[tmux](http://en.wikipedia.org/wiki/Tmux), you end up doing everything
manually. In both cases, there's nothing in place to restart your program if
it crashes. Steve Huffman, one of Reddit's founders, will tell you from
experience [just how bad an idea this
is:](http://www.youtube.com/watch?v=Z9Y__mVExC8)

> In the early days of Reddit, we didn’t really have any crash protection. We
had tons of errors, and Reddit would occasionally lock up, freeze, or get in
an infinite loop, any number of ways of bringing down the site. I used to have
to sleep with my laptop and I would wake up every couple of hours and see if
Reddit was working, and restart it. It was the worst feeling in the world;
totally mentally draining.

To reclaim their ability to sleep, they had the computer keep watch for them,
resurrecting dead processes. This also let them automate health checks: when
Reddit's front page became unresponsive, they could detect this automatically
and simply kill the offending process, secure in the knowledge that the
supervisor program would quickly bring it back up again. They could sleep
right through this fleeting death and rebirth, and only a few people would
notice.

There are some good tools for keeping programs running as daemons, and all of
them make it easy for you. If you're on Ubuntu,
[Upstart](http://upstart.ubuntu.com/) is the default, and it's a good choice.
If you want something less monolithic, [Runit](http://smarden.org/runit/) is
featureful and appealingly Unixy. Several Linux distributions use
[systemd](http://freedesktop.org/wiki/Software/systemd/) as their init, and
for our purposes it's comparable to Upstart. Other interesting options include
[Supervisord](http://supervisord.org/), [God](http://godrb.com/), and
[Circus](http://circus.readthedocs.org/). Unfortunately there is no clear best
answer here, so which one you want depends on your needs. Let's compare.

### Upstart

Used as the default [init](http://en.wikipedia.org/wiki/Init) replacement on
Ubuntu, Upstart is very good at starting things up. [The Upstart
Manual](http://upstart.ubuntu.com/cookbook/) can be kind of overwhelming, but
the config files themselves are pretty simple. For example:

    
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    
    
    # myserver - it's my server
    #
    # myserver listens on a port and, like, does things. Great things.
    
    description     "My cool server"
    
    start on runlevel [2345]
    stop on runlevel [!2345]
    
    respawn
    respawn limit 10 5
    
    setuid myuser
    exec /usr/local/bin/myserver --port 8080

Let's walk through this. The comments at the top customarily include a one-
line description and an optional longer description below - a lot like a git
commit message. The `description` line is, again, for your own use. The `start
on runlevel [2345]` part means to start the service when the machine is
started. The `respawn` and `respawn limit` commands tell Upstart to restart
the server if it dies, but stop trying to restart it after 10 failures in 5
seconds. Finally, Upstart sets the uid to `myuser` and runs the server. Make
sure to `setuid`, because if you don't include it, your server will be run as
root by default. This is a profoundly weird default - it's usually not what
you want, and it's very dangerous. So, be careful with this.

Upstart expects `myserver` not to fork. Any output from `myserver` on stdout
or stderr gets sent, by default, to a log file in `/var/log/upstart/`. Both of
these things can be changed; Upstart can handle the classic forking style of
daemons, and you can plug in other options for logging.

Upstart is one of the best options for running traditional forking daemons,
since it actually uses [ptrace](http://en.wikipedia.org/wiki/Ptrace) to make
sure it's watching the correct process ids. [The system for
this](http://netsplit.com/2007/12/07/how-to-and-why-supervise-forking-
processes/) is very slick.

### Runit

Upstart, while admirably simple to use, does have some shortcomings. Its
default logging, for example, does not support log rotation and is only
compatible with [logrotate](http://linux.die.net/man/8/logrotate) if you use
the `copytruncate` hack. Upstart's options for running servers sometimes don't
include all that you might want; for instance, it currently has no way to
adjust the maximum number of file descriptors, or limit memory usage. Runit is
an interesting alternative, inspired by D. J. Bernstein's
[daemontools](http://cr.yp.to/daemontools.html): it's a collection of programs
which each do one thing, and together, they have a lot of features that
Upstart lacks. If you want a gentle introduction to using Runit, the blog post
[Runit for Ruby (And Everything Else)](http://rubyists.github.com/2011/05/02
/runit-for-ruby-and-everything-else.html) is very well-written.

The two best things about Runit are the
[chpst](http://smarden.org/runit/chpst.8.html) and
[svlogd](http://smarden.org/runit/svlogd.8.html) programs. With `chpst` you
can start a program with a **ch**anged **p**rocess **st**ate:

    
    chpst -u myuser -/ /var/roots/mychroot -o 10000 -n 3 /usr/local/bin/myserver --port 8080
    

This runs the command `/usr/local/bin/myserver --port 8080` as user `myuser`,
chrooted to `/var/roots/mychroot`, with a maximum of 10000 open file
descriptors, at a [niceness](http://en.wikipedia.org/wiki/Nice_(Unix)) level
of 3. Some of these options are available with Upstart, and some aren't. The
beauty of `chpst`, though, is that _you can use it with anything._ You can use
`chpst` with Upstart, or Supervisord, or whatever else strikes your fancy. And
because Runit relies entirely on `chpst` for this sort of thing, you end up
with completely reproducible execution environments: if you run the Runit
script for a service by hand, you get the same environment as you would get if
Runit had run it. This lack of magic is invaluable when debugging.

Similar to `chpst`, Runit's [svlogd](http://smarden.org/runit/svlogd.8.html)
can be used with anything to provide log-handling. It reads text-based logs on
stdin, and either writes that to rotated log files, or sends it to syslog, or
some combination thereof.

Runit is a decent way to run daemons, and it has useful pieces that work with
just about anything. We use it for most of our services. The one caveat to be
aware of is that Runit expects daemons not to fork; this is great for servers
that you write yourself, but it can be inconvenient if you try to use Runit
with some other programs out there.

### systemd

The always-lowercase [systemd](http://freedesktop.org/wiki/Software/systemd/)
is used as the default init on a fair number of Linuxes, most notably the
Redhat/Fedora family of distributions. If you're using one of these and would
like to go with the already-installed option, [this blog post is a good
introduction](http://patrakov.blogspot.com/2011/01/writing-systemd-service-
files.html). Since it's pretty similar to Upstart, the easiest way to describe
it is to talk about some of the differences. First, obviously, the
configuration files have a different syntax, but that's more of a superficial
difference - the concepts are the same. More noteworthy are the ways that
systemd handles logging, resource limiting, and daemons that fork.

Systemd is serious about logging (though I suspect they may have over-
engineered it a bit). By default, all processes have their stdout and stderr
connected to systemd's logging facility (described [here](https://docs.google.
com/document/pub?id=1IC9yOXj7j6cdLLxWEBAGRL6wl97tFxgjLUEHIX3MSTs&pli=1) and
compared with syslog [here](http://blog.gerhards.net/2011/11/serious-syslog-
problems.html)), but it can also go to syslog, the kernel log buffer, a
socket, a tty, or any combination of these. [Docs for the logging are
here](http://www.freedesktop.org/software/systemd/man/systemd-
journald.service.html).

Systemd has a very clever solution to the problem of tracking daemons that
fork, which coincidentally happens to handle resource limiting at the same
time. Where Upstart uses ptrace to watch the forking, systemd runs each daemon
in a [control group](http://en.wikipedia.org/wiki/Cgroups) (requires Linux
2.6.24 or newer) from which it can not escape with any amount of forking. This
allows easy [resource
limiting](http://0pointer.de/blog/projects/resources.html), both for forking
and non-forking daemons, since control groups were made for this sort of
thing.

More documentation can be found [on the systemd web
site](http://www.freedesktop.org/wiki/Software/systemd). Or, for a reference,
[the man pages are
here](http://www.freedesktop.org/software/systemd/man/systemd.html).

### Other contenders: Supervisord, God, Circus

[Supervisord](http://supervisord.org/) is pretty convenient to set up, and
seems to get a fair amount of use in Pythonland. Its main advantage over
Upstart, Runit, and systemd is that Supervisord can easily start pools of
several instances of a program. This is useful if you want a pool of worker
processes, e.g. as FastCGI workers. Supervisord expects programs to not
daemonize, though [it can handle daemonized programs with a pid file
workaround.](http://supervisord.org/subprocess.html#pidproxy-program) This
workaround does not handle edge cases as well as Upstart or systemd, but it
should work fine most of the time.

The hard-to-google [God](http://godrb.com/) hails from the Ruby world. Its
great virtue is programmability: you decide what programs to start, how to run
them, etc., and God will monitor them and keep them up and running. Suppose
you have three back-end web servers, which you want to run on three different
ports, so that a reverse proxy can load-balance across all of them. The config
file is just a Ruby program, so a simple loop can bring up all three servers.
The God web site has examples with code. If you're comfortable writing Ruby
and you have interesting needs, God might be what you're looking for.

Mozilla's [Circus](http://circus.readthedocs.org/) is perhaps the most
interesting of the three. It's programmable in Python, it can handle pools of
workers, and it can [bind sockets for
you](http://circus.readthedocs.org/en/0.6/sockets/). This last point is
especially neat: Circus can act as an HTTP server container by binding to a
socket, starting up a pool of worker processes, and passing them the file
descriptor of the socket. It acts a lot like [Gunicorn](http://gunicorn.org/),
but more powerful. Circus also handles logging and log rotation, has a web-
based console for watching the supervised processes, and sends a stream of all
events over a [ZeroMQ PUB
socket](http://circus.readthedocs.org/en/0.6/design/). It's more actively
developed than Supervisord or God, but already stable enough for production -
and it has the best docs. [Here are some examples of how to use
it.](http://circus.readthedocs.org/en/0.6/usecases/) If I wanted to serve up a
web site with a WSGI stack, Circus is the option I would look at first.

Any of these three can be run under, say, Upstart. They don't want to be an
init replacement, so they play nice with everything else.

## Pick something

Hopefully this infodump is a useful comparison, but the most important thing
isn't _which_ of these options you use; what's important is that you _use
something._ Automating server administration a bit more is almost always a
win. The more things you can solve once and then stop thinking about, the
better.

It's also worth mentioning that a simple crash is far from the only reason why
a daemon might need to be restarted. Several of our server health checks can
kill unresponsive processes, if something has frozen up or gone into a weird
state. This can also be a temporary stopgap against hard-to-debug intermittent
memory leaks: if the server is using too much memory, [kill
it!](http://lwn.net/Articles/317814/) We have an ever-growing number of health
checks keeping our servers in working order, because it's easy to add more.
We're not quite writing [crash-only software](http://en.wikipedia.org/wiki
/Crash-only_software), but it turns out that you can get a lot of its
advantages by making crashes cheap.

(If you find this kind of thing interesting, we're hiring. `curl -L
cueup.com/jobs`)

Mar 8th, 2013

- Peter Scott

[Tweet](http://twitter.com/share)

[« How Our Terminal-Friendly Jobs Page Works](/blog/2013/03/06/how-our-
terminal-friendly-jobs-page-works/)

#

  * [Cue News](http://blog.cueup.com)
  * [Tech Blog Archives](/blog/archives)

# Our GitHub Repos

  * Status updating...
[@Cue on GitHub](https://github.com/Cue)

  * [ ![](http://cueup.com/static/images/v2/header_bar_cue_icon.png) ](http://cueup.com)
  * [Blog](http://blog.cueup.com)
  * [Features](https://www.cueup.com/features)
  * [Jobs](https://www.cueup.com/jobs)
  * [Privacy ](https://www.cueup.com/privacy)
  * [Security](https://www.cueup.com/security)
  * [Terms](https://www.cueup.com/terms)
  * [Support](http://help.cueup.com)
  * [Press](https://www.cueup.com/press)
  * [Twitter](http://www.twitter.com/cueup)
  * [FB](http://www.facebook.com/pages/Cue/108029969265268)

