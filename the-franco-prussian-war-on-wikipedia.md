# Wikimedia Foundation

## Error

Our servers are currently experiencing a technical problem. This is probably
temporary and should be fixed soon. Please [try
again](http://en.wikipedia.org/wiki/Franco-prussian_war) in a few minutes.

You may be able to get further information in the
[#wikipedia](irc://chat.freenode.net/wikipedia) channel on the [Freenode IRC
network](http://www.freenode.net).

The Wikimedia Foundation is a non-profit organisation which hosts some of the
most popular sites on the Internet, including Wikipedia. It has a constant
need to purchase new hardware. If you would like to help, please
[donate](http://wikimediafoundation.org/wiki/Fundraising).

* * *

If you report this error to the Wikimedia System Administrators, please
include the details below.

Request: GET http://en.wikipedia.org/wiki/Franco-prussian_war, from
24.217.135.196 via cp1015.eqiad.wmnet (squid/2.7.STABLE9) to ()

Error: ERR_ACCESS_DENIED, errno [No Error] at Sat, 09 Mar 2013 01:24:11 GMT

